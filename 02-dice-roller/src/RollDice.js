import React, { Component } from "react";
import "./RollDice.css";
import Die from "./Die.js";

class RollDice extends Component {
  static defaultProps = {
    sides: ["one", "two", "three", "four", "five", "six"],
  };
  constructor(props) {
    super(props);
    this.state = { die1: "one", die2: "one", isRolling: false };
    this.roll = this.roll.bind(this);
  }
  roll() {
    // Pick 2 new rolls
    const newDie1 = this.props.sides[
      Math.floor(Math.random() * this.props.sides.length)
    ];

    const newDie2 = this.props.sides[
      Math.floor(Math.random() * this.props.sides.length)
    ];

    // Set state with new rolls
    this.setState({ die1: newDie1, die2: newDie2, isRolling: true });

    // Wait one second and set isRolling to false again
    setTimeout(() => {
      this.setState({isRolling : false});
    }, 1000);
  }
  render() {
    return (
      <div class="RollDice">
        <div class="RollDice-Container">
          <Die face={this.state.die1} rolling={this.state.isRolling}/>
          <Die face={this.state.die2} rolling={this.state.isRolling}/>
        </div>
        <button onClick={this.roll} disabled={this.state.isRolling}>
          {this.state.isRolling ? "Rolling..." : "Roll Dice!"}
        </button>
      </div>
    );
  }
}

export default RollDice;
