import React from 'react';
import './App.css';
import Toggler from './Toggler';
import SimpleFormClass from "./SimpleFormClass";
import SimpleFormHooks from "./SimpleFormHooks";
import SimpleFormInputHook from "./SimpleFormInputHook";

function App() {
  return (
    <div className="App">
      <Toggler />
      <SimpleFormClass />
      <SimpleFormHooks />
      <SimpleFormInputHook />
    </div>
  );
}

export default App;
