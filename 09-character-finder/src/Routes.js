import React, { Component } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import CharList from "./CharList";
import CharDetails from "./CharDetails";

class Routes extends Component {
  render() {
    const getChar = (props) => {
      let name = props.match.params.name;
      let currentChar = this.props.chars.find(
        (char) => char.name.toLowerCase() === name.toLowerCase()
      );
      if (currentChar !== undefined) {
        return <CharDetails {...props} char={currentChar} />;
      } else {
        return <Redirect to="/chars" />;
      }
    };

    return (
      <Switch>
        <Route
          exact
          path="/chars"
          render={() => <CharList chars={this.props.chars} />}
        />
        <Route exact path="/chars/:name" render={getChar} />
        <Redirect to="/chars" />
      </Switch>
    );
  }
}

export default Routes;
