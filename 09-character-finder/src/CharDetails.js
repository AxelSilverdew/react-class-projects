import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./CharDetails.css";

class CharDetails extends Component {
  render() {
    let { char } = this.props;
    return (
      <div className="CharDetails row justify-content-center mt-5">
        <div className="col-11 col-lg-5">
          <div className="CharDetails-card card">
            <img className="card-img-top" src={char.src} alt={char.name} />
            <div className="card-body">
              <h2 className="card-title">{char.name}</h2>
              <h4 className="card-subtitle text-muted">{char.age} years old</h4>
            </div>
            <ul className="list-group list-group-flush">
              {char.facts.map((fact, idx) => (
                <li className="list-group-item" key={idx}>
                  {fact}
                </li>
              ))}
            </ul>
            <div className="card-body">
              <Link to="/chars" className="btn btn-info">
                Go Back
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CharDetails;
