import React, { Component } from "react";
import Routes from "./Routes";
import Navbar from "./Navbar";
import "./App.css";
import spiderman from "./spiderman.png";
import deku from "./deku.png";
import hirotaka from "./hirotaka.png";

class App extends Component {
  static defaultProps = {
    characters: [
      {
        name: "Spider-man",
        age: 17,
        src: spiderman,
        facts: [
          "His name is Miles Morales",
          "He lives in Harlem, NY",
          "He can shock people by touching them",
        ],
      },
      {
        name: "Deku",
        age: 16,
        src: deku,
        facts: [
          "His real name is Izuku Midoriya",
          "He inherited his powers from All Might",
          "He favorite food is Katsudon",
        ],
      },
      {
        name: "Hirotaka",
        age: 26,
        src: hirotaka,
        facts: [
          "His full name is Hirotaka Nifuji",
          "He's a Pisces",
          "He's obsessed with video games",
        ],
      },
    ],
  };
  render() {
    return (
      <div>
        <Navbar chars={this.props.characters} />
        <div className="container">
          <Routes chars={this.props.characters} />
        </div>
      </div>
    );
  }
}

export default App;
