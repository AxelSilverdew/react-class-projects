import React, { Component } from "react";
import { NavLink, Link } from "react-router-dom";

class Navbar extends Component {
  render() {
    const charLinks = this.props.chars.map((char) => (
      <li className="nav-item" key={char.name}>
        <NavLink exact to={`/chars/${char.name}`} className="nav-link">
          {char.name}
        </NavLink>
      </li>
    ));
    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <Link className="navbar-brand" to="/chars">
          Char App
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-bs-toggle="collapse"
          data-bs-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <NavLink exact to="/chars" className="nav-link">
                Home
              </NavLink>
            </li>
            {charLinks}
          </ul>
        </div>
      </nav>
    );
  }
}

export default Navbar;
