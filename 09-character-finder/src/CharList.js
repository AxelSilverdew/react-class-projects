import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./CharList.css";
class CharList extends Component {
  render() {
    return (
      <div className="CharList">
        <h1 className="display-1 text-center my-5">Characters!</h1>
        <div className="row">
          {this.props.chars.map((c) => (
            <div className="CharList-char col-lg-4 text-center" key={c.name}>
              <img src={c.src} alt={c.name} />
              <h3>
                <Link className="underline" to={`/chars/${c.name}`}>{c.name}</Link>
              </h3>
            </div>
          ))}
        </div>
      </div>
    );
  }
}

export default CharList;
